#!/bin/bash

# There is no deployment pipeline for this script, it should be run manually
# This script contains code to create storage account which will store terraform state
# It should be run only once in case there is no existing storage account created in Azure
# To run it, first login to azure with your account via 'az login'


# names must match the names in providers.tf
RG_NAME="rsg-tfoicd-shared"
SA_NAME="satfoicdshared"
CONTAINER_NAME="terraformstate"
tags="env=terraform"

# create a resource group
az group create -n $RG_NAME -l germanywestcentral --tags $tags

# create a storage account for storing terraform state
az storage account create -n $SA_NAME -g $RG_NAME -l germanywestcentral --sku Standard_LRS --tags $tags

KEY=$(az storage account keys list -g $RG_NAME -n $SA_NAME --query "[0].value" -o tsv)

az storage container create -n $CONTAINER_NAME --account-name $SA_NAME --account-key $KEY