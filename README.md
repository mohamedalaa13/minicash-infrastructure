# minicash-infra
Main repository for Minicash infrastructure as code and related documentation

This repo contains infrastrucure for Minicash2 application. It consists of terraform and kubernetes.

Terraform

Terraform folder consists of modules directory and one directory per environment. Note that adding any new environment will require to make a copy of environment directory.

Modules:
    This repo contains module for deploying Azure Kubernetes Service

Environment dirs:
    - disk_encryption_set.tf - contains code for encrypting disks on nodes
    - key_vault_cluster_.tf - contains azure keyvault where app secrets reside
    - main.tf - contains AKS cluster code and log analytics
    - outputs.tf - terraform output information
    - postgresql.tf - contains Flexible server for Postrgress deployment
    - providers.tf - terraform provider information
    - terraform.tfvars - variables for current environment
    - variables.tf - list of used variables with explanation

Deploying an environment
To create an environment we are using Azure Service Principle. The service principle and the azure storage for keeping terraform state file must bre pre-deployed before using terraform (done). To use the SP we must set the variables for terraform to read and be able to use the SP. Variables needed are:

export TF_VAR_client_id="" && \
export TF_VAR_client_secret="" && \
export ARM_SUBSCRIPTION_ID="f2a04150-5794-4841-a00d-60f9b6974635" && \
export ARM_CLIENT_ID="" && \
export ARM_CLIENT_SECRET="" && \ 
export ARM_TENANT_ID="58191332-4582-42f1-a685-f77f77def707"

Required secrets are stored in Azure keyvault - xxxx-xxxx-xxxx

When these variables are set, make sure to logout from Azure cli, or else the deployment will be executed using your user.

Terraform deployment

1. Set the variables
2. Az logout
3. terraform init
4. terraform apply (check the deployment plan before confirming apply)
6. wait for the deployment to finish

Kubernetes deployment

1. Login to btech bastion vm (this is needed because the cluster is private, there are no public endpoints available)
2. Go to Azure portal > Kubernetes services > Select your new cluster
3. Go to Connect button, copy the first command and execute
4. Copy the second command and append " --admin" at the end and execute
5. Deploy ingress controller

# create a acr-pull-secret inside of ingress-main namespace (it has to be deployed into same namespace where ingress controller will reside):
# use SP id and pass
kubectl create secret docker-registry acr-pull-secret \
    --docker-server=btechorg.azurecr.io \
    --docker-username= \
    --docker-password= \
    -n ingress-main

REGISTRY_NAME=btechorg
SOURCE_REGISTRY=k8s.gcr.io
CONTROLLER_IMAGE=ingress-nginx/controller
CONTROLLER_TAG=v1.2.1
PATCH_IMAGE=ingress-nginx/kube-webhook-certgen
PATCH_TAG=v1.1.1
DEFAULTBACKEND_IMAGE=defaultbackend-amd64
DEFAULTBACKEND_TAG=1.5

az acr import --name $REGISTRY_NAME --source $SOURCE_REGISTRY/$CONTROLLER_IMAGE:$CONTROLLER_TAG --image $CONTROLLER_IMAGE:$CONTROLLER_TAG
az acr import --name $REGISTRY_NAME --source $SOURCE_REGISTRY/$PATCH_IMAGE:$PATCH_TAG --image $PATCH_IMAGE:$PATCH_TAG
az acr import --name $REGISTRY_NAME --source $SOURCE_REGISTRY/$DEFAULTBACKEND_IMAGE:$DEFAULTBACKEND_TAG --image $DEFAULTBACKEND_IMAGE:$DEFAULTBACKEND_TAG

# Add the ingress-nginx repository
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

# Set variable for ACR location to use for pulling images
ACR_URL=btechorg.azurecr.io

# Use Helm to deploy an NGINX ingress controller
helm install ingress-nginx ingress-nginx/ingress-nginx \
    --version 4.1.3 \
    --namespace ingress-main \
    --create-namespace \
    --set controller.replicaCount=2 \
    --set controller.nodeSelector."kubernetes\.io/os"=linux \
    --set controller.image.registry=$ACR_URL \
    --set controller.image.image=$CONTROLLER_IMAGE \
    --set controller.image.tag=$CONTROLLER_TAG \
    --set controller.image.digest="" \
    --set controller.admissionWebhooks.patch.nodeSelector."kubernetes\.io/os"=linux \
    --set controller.service.annotations."service\.beta\.kubernetes\.io/azure-load-balancer-health-probe-request-path"=/healthz \
    --set controller.admissionWebhooks.patch.image.registry=$ACR_URL \
    --set controller.admissionWebhooks.patch.image.image=$PATCH_IMAGE \
    --set controller.admissionWebhooks.patch.image.tag=$PATCH_TAG \
    --set controller.admissionWebhooks.patch.image.digest="" \
    --set defaultBackend.nodeSelector."kubernetes\.io/os"=linux \
    --set defaultBackend.image.registry=$ACR_URL \
    --set defaultBackend.image.image=$DEFAULTBACKEND_IMAGE \
    --set defaultBackend.image.tag=$DEFAULTBACKEND_TAG \
    --set defaultBackend.image.digest="" \
    --set imagePullSecrets[0].name=acr-pull-secret \
    -f internal-ingress-controler.yaml

Find the managed identity terraform has created to your environment, go to vnet-minicash2-spoke vnet and in IAM roles give it a Network Contributor role
# After this, managed identity that AKS created must have a Network Contributor role on the vnet where IP is taken from or it will be sutuck in <pending> mode
Go to your newly created resource group for the cluster rgs-minicash2-k8s-node-<ENV> and find the VM scale set aks-nodepool-********-vmss. Add Owner role to Managed identity

6. Deploy Internal ingress controler. In this repo /kubernetes/ENV execute kubectl apply -f internal-ingress-controler.yaml (make sure to edit the file first (add ip address))
7. Deploy secret provider for AKS. In this repo /kubernetes/ENV execute kubectl apply -f secretproviderclass.yaml
8. Deploy services and deployments. In this repo /kubernetes/ENV execute kubectl apply -f deployment.yaml
9. Deploy ingress rules. In this repo /kubernetes/ENV execute kubectl apply -f ingress.yaml
10. Deploy roles and rolebindings. In this repo /kubernetes/ENV execute kubectl apply -f role.yaml and kubectl apply -f rolebinding.yaml (make sure to change the id-s in files)

11. Go to resource group which holds the nodes rgs-minicash2-k8s-node-<env> and find the VSS routing table. Add the following routes:
    1. Name:default route, Address prefix: 0.0.0.0/0, Next hop type: VirtualAppliance, Next hop IP address: 10.5.201.4
    2. Name:to-hub, Address prefix: 0.0.0.0/0, Next hop type: VirtualAppliance, Next hop IP address: 10.5.201.4