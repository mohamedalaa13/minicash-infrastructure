resource "azurerm_resource_group" "rg-bastion-vm" {
  name     = "rgs-minicash2-bastionvm-prod"
  location = var.location
}

resource "azurerm_subnet" "sn-bastion" {
  address_prefixes     = ["10.5.52.128/26"]
  name                 = "sn-bastion-${var.env}-${random_id.prefix.hex}"
  resource_group_name  = var.resource_group_name
  virtual_network_name = var.vnet_name
}

# resource "azurerm_public_ip" "pip-bastion" {
#   name                = "${var.prefix}-bastion-pip"
#   resource_group_name = azurerm_resource_group.rg-bastion-vm.name
#   location            = azurerm_resource_group.rg-bastion-vm.location
#   allocation_method   = "Static"
# }

resource "azurerm_network_interface" "bastionvm-main" {
  name                = "${var.prefix}-bastion-nic"
  resource_group_name = azurerm_resource_group.rg-bastion-vm.name
  location            = azurerm_resource_group.rg-bastion-vm.location

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.sn-bastion.id
    private_ip_address_allocation = "Dynamic"
    # public_ip_address_id          = azurerm_public_ip.pip-bastion.id
  }
}

# resource "azurerm_network_interface" "bastionvm-internal" {
#   name                = "${var.prefix}-nic2"
#   resource_group_name = azurerm_resource_group.rg-bastion-vm.name
#   location            = azurerm_resource_group.rg-bastion-vm.location

#   ip_configuration {
#     name                          = "internal"
#     subnet_id                     = azurerm_subnet.sn-bastion.id
#     private_ip_address_allocation = "Dynamic"
#   }
# }

resource "azurerm_network_security_group" "nsg-bastion-ssh" {
  name                = "nsg-bastion-ssh"
  location            = azurerm_resource_group.rg-bastion-vm.location
  resource_group_name = azurerm_resource_group.rg-bastion-vm.name
  security_rule {
    access                     = "Allow"
    direction                  = "Inbound"
    name                       = "SSH"
    priority                   = 100
    protocol                   = "Tcp"
    source_port_range          = "*"
    source_address_prefix      = "*"
    destination_port_range     = "22"
    destination_address_prefix = azurerm_network_interface.bastionvm-main.private_ip_address
  }
}

resource "azurerm_network_interface_security_group_association" "main" {
  network_interface_id      = azurerm_network_interface.bastionvm-main.id
  network_security_group_id = azurerm_network_security_group.nsg-bastion-ssh.id
}

resource "azurerm_linux_virtual_machine" "bastionvm" {
  name                = "${var.prefix}-bastion-vm"
  resource_group_name = azurerm_resource_group.rg-bastion-vm.name
  location            = azurerm_resource_group.rg-bastion-vm.location
  size                = "Standard_B2s"
  admin_username      = "adminuser"
  provision_vm_agent  = true

  identity {
    type   = "SystemAssigned"
  }

  network_interface_ids = [
    azurerm_network_interface.bastionvm-main.id,
    # azurerm_network_interface.bastionvm-internal.id
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    offer     = "0001-com-ubuntu-server-focal"
    publisher = "Canonical"
    sku       = "20_04-lts-gen2"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }
}
