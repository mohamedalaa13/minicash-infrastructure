resource "random_password" "pgsql-admin-password" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "azurerm_resource_group" "rg-database-prod" {
  name     = var.resource_group_name_database
  location = var.location
  tags     = var.tags
}

resource "azurerm_subnet" "sn-pgsql-prod" {
  name                 = "sn-pgsql-${var.env}-${random_id.prefix.hex}"
  resource_group_name  = var.resource_group_name
  virtual_network_name = var.vnet_name
  address_prefixes     = ["10.5.52.64/26"]
  service_endpoints    = ["Microsoft.Storage", "Microsoft.AzureActiveDirectory", "Microsoft.KeyVault"]
  delegation {
    name = "Microsoft.DBforPostgreSQL/flexibleServers"
    service_delegation {
      name = "Microsoft.DBforPostgreSQL/flexibleServers"
      actions = [
        "Microsoft.Network/virtualNetworks/subnets/join/action",
      ]
    }
  }
}
resource "azurerm_private_dns_zone" "dnszone-pgsql" {
  name                = "pgsql.postgres.database.azure.com"
  resource_group_name = azurerm_resource_group.rg-database-prod.name
  tags                = var.tags
}

resource "azurerm_private_dns_zone_virtual_network_link" "dnsprivatenetworklink-prod" {
  name                  = "btechmc2.local" #TO BE CHANGED @TODO
  private_dns_zone_name = azurerm_private_dns_zone.dnszone-pgsql.name
  virtual_network_id    = "/subscriptions/f2a04150-5794-4841-a00d-60f9b6974635/resourceGroups/rgs-minicash2-k8s-prod/providers/Microsoft.Network/virtualNetworks/vnet-minicash2-spoke"
  resource_group_name   = azurerm_resource_group.rg-database-prod.name
  tags                  = var.tags
}

resource "azurerm_postgresql_flexible_server" "pfs-prod" {
  name                   = "psqlflexibleserver-${lower(var.prefix)}-${lower(var.env)}"
  resource_group_name    = azurerm_resource_group.rg-database-prod.name
  location               = azurerm_resource_group.rg-database-prod.location
  version                = "12"
  delegated_subnet_id    = azurerm_subnet.sn-pgsql-prod.id
  private_dns_zone_id    = azurerm_private_dns_zone.dnszone-pgsql.id
  administrator_login    = "${var.prefix}psqladmin${var.env}"
  administrator_password = random_password.pgsql-admin-password.result
  zone = "1"

  storage_mb = 32768

  sku_name   = "GP_Standard_D2s_v3"
  depends_on = [azurerm_private_dns_zone_virtual_network_link.dnsprivatenetworklink-prod]
  tags       = var.tags

}

output "psql_admin_user" {
  description = "Flexible Postgresql admin username"
  sensitive   = true
  value       = azurerm_postgresql_flexible_server.pfs-prod.administrator_login
}

output "psql_admin_password" {
  description = "Flexible Postgresql admin password"
  sensitive   = true
  value       = azurerm_postgresql_flexible_server.pfs-prod.administrator_password
}
