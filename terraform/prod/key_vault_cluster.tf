resource "azurerm_key_vault" "vault_cluster" {
  location                    = local.resource_group.location
  name                        = "aks-keyvault-${var.env}-${random_string.key_vault_prefix.result}"
  resource_group_name         = local.resource_group.name
  sku_name                    = "premium"
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  purge_protection_enabled    = true
  soft_delete_retention_days  = 7

  network_acls {
    bypass         = "AzureServices"
    default_action = "Allow"
    ip_rules       = [local.public_ip]
  }
  tags       = var.tags
}

resource "azurerm_key_vault_access_policy" "cluster-keyvault-terraform-mc-sp" {
  key_vault_id = azurerm_key_vault.vault_cluster.id
  object_id    = "05b5a725-bc79-4234-98b5-12fcb66d796d"
  tenant_id    = data.azurerm_client_config.current.tenant_id
  key_permissions = [
    "Get",
    "Create",
    "Delete",
  ]
}

resource "azurerm_key_vault_access_policy" "cluster-keyvault-terraform-azure" {
  key_vault_id = azurerm_key_vault.vault_cluster.id
  object_id    = "5a3d63f3-9186-489b-880b-1e3024733fb9"
  tenant_id    = data.azurerm_client_config.current.tenant_id
  key_permissions = [
    "Get",
    "Create",
    "Delete",
  ]
}