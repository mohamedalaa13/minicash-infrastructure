# PROJECT CONF
prefix                = "minicash2"
env                   = "stg"
create_resource_group = false # Resource group is precreated
location              = "westeurope"
resource_group_name   = "rgs-minicash2-k8s-stg"                # Resource group for AKS (MAIN RG)
vnet_name             = "vnet-minicash2-spoke"                 # Name of the virutal network precreated manualy for spoke
vnet_id               = "5acdbc87-b095-48ce-a7c3-0c5268aed174" # ID of the virutal network precreated manualy for spoke

# CLUSTER CONF
node_resource_group                = "rgs-minicash2-k8s-node-stg"
kubernetes_version                 = "1.25.5"
agents_count                       = 2
agents_availability_zones          = ["1", "2"]
tags                               = { "project" = "minicash2", "env" = "stg" }
rbac_aad_admin_group_object_ids    = ["5bd61ce8-76d7-4f99-bcbc-cdc8d11f0499"] # ID of the admin group for AKS cluster
key_vault_secrets_provider_enabled = true
agents_size                        = "Standard_D2s_v3"

# DATABASE CONF
resource_group_name_database = "rgs-minicash2-pgsql-stg"
