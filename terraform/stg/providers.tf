terraform {
  required_version = ">= 1.2"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.40, < 4.0"
    }
    curl = {
      source  = "anschoewe/curl"
      version = "1.0.2"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.3.2"
    }
    tls = {
      source  = "hashicorp/tls"
      version = ">= 3.1"
    }
  }

  backend "azurerm" {
    resource_group_name  = "rsg-btech-mc2-seed"
    storage_account_name = "samc2terraform"
    container_name       = "terraformstate"
    key                  = "stg.terraformstate"
    client_id            = "5a3d63f3-9186-489b-880b-1e3024733fb9"
  }
}

provider "azurerm" {
  #  use_oidc = true
  features {
    key_vault {
      purge_soft_delete_on_destroy       = false
      purge_soft_deleted_keys_on_destroy = false
      recover_soft_deleted_key_vaults    = false
    }
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

provider "curl" {}

provider "random" {}
