resource "random_id" "prefix" {
  byte_length = 8
}

resource "azurerm_resource_group" "main" {
  count = var.create_resource_group ? 1 : 0

  location = var.location
  name     = var.resource_group_name
  tags     = var.tags
}

locals {
  resource_group = {
    name     = var.create_resource_group ? azurerm_resource_group.main[0].name : var.resource_group_name
    location = var.location
  }
}

resource "azurerm_subnet" "sn-aks-stg" {
  address_prefixes                          = ["10.5.53.0/26"]
  name                                      = "sn-aks-${var.env}-${random_id.prefix.hex}"
  resource_group_name                       = "rgs-minicash2-k8s-prod"
  virtual_network_name                      = var.vnet_name
  private_endpoint_network_policies_enabled = true
}

resource "azurerm_user_assigned_identity" "aid-stg" {
  location            = local.resource_group.location
  name                = "aks-man-id-${var.env}-${random_id.prefix.hex}"
  resource_group_name = local.resource_group.name
}

# Azure Log Analytivs workspace
resource "azurerm_log_analytics_workspace" "main" {
  location            = local.resource_group.location
  name                = "workspace-${var.prefix}-${var.env}"
  resource_group_name = local.resource_group.name
  retention_in_days   = 30
  sku                 = "PerGB2018"
  tags                = var.tags
}

module "aks_cluster" {
  source = "../modules/aks"

  prefix                               = var.prefix
  resource_group_name                  = local.resource_group.name
  location                             = var.location
  vnet_subnet_id                       = azurerm_subnet.sn-aks-stg.id
  admin_username                       = null
  azure_policy_enabled                 = true
  cluster_log_analytics_workspace_name = "workspace-${var.prefix}-${var.env}"
  cluster_name                         = "${var.prefix}-k8s-${var.env}"
  kubernetes_version                   = var.kubernetes_version
  node_resource_group                  = var.node_resource_group
  agents_count                         = var.agents_count
  client_id                            = var.client_id
  client_secret                        = var.client_secret
  agents_max_pods                      = var.agents_max_pods
  agents_availability_zones            = var.agents_availability_zones
  disk_encryption_set_id               = azurerm_disk_encryption_set.des.id
  identity_ids                         = [azurerm_user_assigned_identity.aid-stg.id]
  identity_type                        = "UserAssigned"
  log_analytics_workspace_enabled      = true
  tags                                 = var.tags
  rbac_aad_admin_group_object_ids      = var.rbac_aad_admin_group_object_ids
  key_vault_secrets_provider_enabled   = var.key_vault_secrets_provider_enabled
  enable_host_encryption               = true
  log_analytics_workspace = {
    id   = azurerm_log_analytics_workspace.main.id
    name = azurerm_log_analytics_workspace.main.name
  }
  maintenance_window = {
    allowed = [
      {
        day   = "Sunday",
        hours = [22, 23]
      },
    ]
    not_allowed = []
  }
  # net_profile_pod_cidr              = "192.168.0.0/16"
  private_cluster_enabled           = true
  rbac_aad                          = true
  rbac_aad_managed                  = true
  role_based_access_control_enabled = true
  sku_tier                          = "Paid"
}
